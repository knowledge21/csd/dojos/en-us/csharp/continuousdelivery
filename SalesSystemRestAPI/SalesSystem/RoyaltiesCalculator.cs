﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesSystem
{
    public class RoyaltiesCalculator
    {
        private ComissionCalculator comissionCalculator;
        private SalesRepository salesRepository;

        public RoyaltiesCalculator()
        {
            comissionCalculator = new ComissionCalculator();
            salesRepository = new SalesRepository();
        }

        public RoyaltiesCalculator(SalesRepository salesRepository)
        {
            this.comissionCalculator = new ComissionCalculator();
            this.salesRepository = salesRepository;
        }

        public double Calculate(int month, int year)
        {
            double royalties = 0.0;

            List<Sale> sales = salesRepository.GetSales(month, year);

            if (sales != null)
            {
                foreach (var sale in sales)
                {
                    var value = sale.value - comissionCalculator.Calculate(sale.value);
                    royalties += value * 0.2;
                }
            }

            return TruncateTwoDecimalPlaces(royalties);
        }

        private double TruncateTwoDecimalPlaces(double value)
        {
            return Math.Floor(value * 100) / 100;
        }
    }
}